$(document).ready(function(){
  if ($(this).scrollTop() > 10) {
    stickyHeader();
  }
	$(window).on('scroll', stickyHeader);
});

function stickyHeader() {
		if ($(this).scrollTop() > 10) {
			$('.header').addClass("sticky");
		} else {
			$('.header').removeClass("sticky");
		}
}