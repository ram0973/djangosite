��          �      L      �  9   �  e   �  4   a     �     �     �     �     �  �   �  %   o     �     �     �  
   �  	   �     �     �     �  �  �  `   �  �     R   �     �       '        D  -   Y  _   �  %   �               <     X     _     n     �     �     	                    
                                                                           Designates whether the user can log into this admin site. Designates whether this user should be treated as active. Unselect this instead of deleting accounts. Enter the same password as before, for verification. Important dates Password Password confirmation Permissions Personal info Raw passwords are not stored, so there is no way to see this user's password, but you can change the password using <a href="{}">this form</a>. The two password fields didn't match. active date joined email address first name last name staff status user users Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || (n%100>=11 && n%100<=14)? 2 : 3);
 Определяет, может ли пользователь входить в админку. Определяет, активен ли пользователь. Отключите это вместо удаления пользователя Введите тот же пароль повторно, для проверки. Важные даты Пароль Подтверждение пароля Разрешения Персональная информация Можно изменить пароль используя эту <a href="{}">форму</a>. Пароли не совпадают. активно дата регистрации почтовый адрес имя фамилия администрация пользователь пользователи 