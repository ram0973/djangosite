#!/usr/bin/env bash
bash ./scripts/install-docker.sh
bash ./scripts/install-pyenv.sh
bash ./scripts/install-poetry.sh
poetry install
poetry shell
fab --print-completion-script=bash | sudo tee /etc/bash_completion.d/fabric > /dev/null
