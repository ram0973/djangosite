SHELL := /bin/bash

.PHONY: help
help:
	make -h

.PHONY: build
build:
	docker-compose build --parallel

.PHONY: build-prod
build-prod:
	docker-compose build --parallel -f docker-compose.yml -f docker-production.yml

.PHONY: collectstatic
collectstatic:
	python manage.py collectstatic --noinput

.PHONY: compilemessages
compilemessages:
	python manage.py compilemessages --locale=ru

.PHONY: createsuperuser
createsuperuser:
	python manage.py createsuperuser

.PHONY: functests
functests:
	pytest -k "functional_test.py" --cov=.

.PHONY: journal-today
journal-today:
	sudo journalctl -b | tac | less

.PHONY: log-nginx
log-nginx:
	docker-compose run nginx tail -n20 /var/log/nginx/error.log

.PHONY: log-nginx-nodocker
log-nginx-nodocker:
	tail -n20 /var/log/nginx/error.log

.PHONY: log-psql
log-psql:
	docker-compose run tail -n20 /var/log/postgresql/postgresql-10-main.log

.PHONY: makemessages
messages:
	python manage.py makemessages -a

.PHONY: migrate
migrate:
	python manage.py migrate

.PHONY: makemigrations
makemigrations:
	python manage.py makemigrations

.PHONY: runserver
runserver:
	python manage.py runserver 0.0.0.0:8000

.PHONY: runserver-plus
runserver-plus:
	python manage.py runserver_plus 0.0.0.0:8000

.PHONY: tests
tests:
	pytest --cov=.

.PHONY: unittests
unittests:
	DJANGO_SETTINGS_MODULE=core.settings
	DJANGO_CONFIGURATION=Staging
	pytest -k "not test_functional.py" --cov=.
