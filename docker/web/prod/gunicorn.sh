#!/usr/bin/env bash

# Exit on error. Append "|| true" if you expect an error.
set -o errexit
# Exit on error inside any functions or subshells.
set -o errtrace
# Do not allow use of undefined vars. Use ${VAR:-} to use an undefined VAR
# set -o nounset
# Catch the error in case mysqldump fails (but gzip succeeds) in `mysqldump |gzip`
set -o pipefail
# Turn on traces, useful while debugging but commented out by default
# set -o xtrace

# http://docs.gunicorn.org/en/stable/configure.html
#export DJANGO_CONFIGURATION

/venv/bin/gunicorn core.wsgi \
--workers=3 \
--max-requests=2000 \
--max-requests-jitter=400 \
--bind=0.0.0.0:8000 \
--chdir=/src \
--worker-tmp-dir=/dev/shm \
--log-level=info \
--log-file=-
#--access-logfile=/src/access.log \
#--error-logfile=/src/error.log
